import airsim
import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
import os
import sys

#keyboard input and display
import pygame
import pygame.locals

def gray(im):
    im = 255 * (im / im.max())
    w, h = im.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 2] = ret[:, :, 1] = ret[:, :, 0] = im
    return ret

class Episode:
    def __init__(self,target='airsim',action='model',model=None,display_current_stat=False,dt=0.1):
        self.target = target
        self.action = action
        self.model = model
        self.display_current_stat = display_current_stat
        self.airsim_client = None
        self.airsim_car = None
        
        self.clear()
        self.dt = dt
    
    @staticmethod
    def loadSize(dir_name):
        df = pd.read_csv('{}/data-scalars.csv'.format(dir_name))
        return len(df.index)

    @staticmethod
    def loadVectorizedShape(dir_name):
        a = np.load('{}/vectorized-graphics.npz'.format(dir_name))
        return a.f.A.shape[1:]

    @staticmethod
    def loadShape(dir_name):
        a = np.load('{}/graphics.npz'.format(dir_name))
        return a.f.A.shape[1:]
    
    def clear(self):
        self.stats_graphics = np.array([])
        self.stats_vectorized_graphics = np.array([])
        self.stats_sensors = np.array([])
        self.actions = np.array([])
        self.time = np.array([])

    def vectorize(self):
        self.stats_vectorized_graphics = self.model.model_encoder.predict(x=[self.stats_graphics[:,:,:,:3],self.stats_graphics[:,:,:,3]])
    
    def unvectorize(self):
        self.stats_graphics = self.model.model_decoder.predict(x=self.stats_vectorized_graphics)

    def stateCount(self):
        return self.time.shape[0]
    
    def save(self,dir_name):
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        
        sensors_and_actions = np.concatenate([self.time.reshape((-1,1)),self.stats_sensors,self.actions],axis=1)
        df=pd.DataFrame(sensors_and_actions, columns = ['time', 'speed', 'collision', 'RPM', 'gear','throttle','breaking','steering'])
        df.to_csv('{}/data-scalars.csv'.format(dir_name))

        if len(self.stats_graphics)>0:
            np.savez('{}/graphics.npz'.format(dir_name),A=self.stats_graphics)

        if len(self.stats_vectorized_graphics)>0:
            np.savez('{}/vectorized-graphics.npz'.format(dir_name),A=self.stats_vectorized_graphics)

    def load(self,dir_name):
        sensors_and_actions = pd.read_csv('{}/data-scalars.csv'.format(dir_name))

        self.time = np.array(sensors_and_actions[['time']])
        self.stats_sensors = np.array(sensors_and_actions[['speed', 'collision', 'RPM', 'gear']])
        self.actions = np.array(sensors_and_actions[['throttle','breaking','steering']])
        
        if os.path.isfile('{}/graphics.npz'.format(dir_name)):
            graphics=np.load('{}/graphics.npz'.format(dir_name))
            self.stats_graphics=graphics.f.A

        if os.path.isfile('{}/vectorized-graphics.npz'.format(dir_name)):
            vectorized_graphics=self.stats_vectorized_graphics=np.load('{}/vectorized-graphics.npz'.format(dir_name))
            self.stats_vectorized_graphics=vectorized_graphics.f.A
    
    def getAction(self, sensors, vectorized_graphics=None):
        accel,breaking,steering=0,0,0

        if self.action == 'keyboard':

            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_UP]:
                accel = 1
                breaking = 0
            elif pressed[pygame.K_DOWN]:
                accel = 0
                breaking = 1
            if pressed[pygame.K_RIGHT]:
                steering = 0.5
            elif pressed[pygame.K_LEFT]:
                steering = -0.5

            for event in pygame.event.get():
                if event.type == pygame.locals.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                elif event.type == pygame.locals.QUIT:
                    pygame.quit()
                    sys.exit()
            
        elif self.action == 'model':
            q_out = self.model.predict([vectorized_graphics, sensors[0], sensors[1], sensors[2], sensors[3]])
            accel=q_out[0]
            breaking=q_out[1]
            steering=[-1,-0.5,0,0.5,1][np.argmax(q_out[2:])]
        else:
            print('ERROR action can\'t be generated from {}'.format(self.action))

        return accel,breaking,steering

    def generateInput(self):
        if self.target == 'airsim':
            responses = self.airsim_client.simGetImages([
                airsim.ImageRequest(0, airsim.ImageType.DepthVis,True),
                airsim.ImageRequest(1, airsim.ImageType.Scene,False,False)]) 

            np_array=np.array(responses[0].image_data_float, dtype=np.float32)
            img_f = np.clip(np_array.reshape(responses[0].height, responses[0].width) * 255, a_min = 0, a_max = 255).astype(dtype=np.uint8).reshape((responses[0].height,responses[0].width,1))
            
            img1d = np.fromstring(responses[1].image_data_uint8, dtype=np.uint8)
            img_rgb = img1d.reshape(responses[1].height, responses[1].width, 4)[:,:,:3]

            return np.concatenate([img_rgb, img_f],axis=2)
        elif self.action == 'model':
            print('ERROR input shoudn\'t be generated from  {} (use vectorized input directly)'.format(self.action))
        else:
            print('ERROR input can\'t be generated from {}'.format(self.action))
    
    def display_replay(self):

        pygame.init()

        window = pygame.display.set_mode((256*2, 144*4), 0, 32)
        window.fill((0,0,0))

        state_count = self.time.shape[0]
        for i in range(state_count):
            speed, collision, rpm, gear=self.stats_sensors[i][0],self.stats_sensors[i][1],self.stats_sensors[i][2],self.stats_sensors[i][3]
            self.display_stat(pygame, window, speed, collision, rpm, gear,self.stats_graphics[i,:,:,:])

            for event in pygame.event.get():
                if event.type == pygame.locals.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                elif event.type == pygame.locals.QUIT:
                    pygame.quit()
                    sys.exit()

            if (i+1) < state_count:
                pygame.time.delay(int((self.time[i+1]-self.time[i])*1000))
        
        pygame.quit()
    
    def display_stat(self, pygame, window, speed, collision, rpm, gear, graphics=None):
        accel,breaking,steering=0,0,0

        img = pygame.surfarray.make_surface(graphics[:,:,:3])
        img = pygame.transform.rotate(img, 90)
        img = pygame.transform.flip(img, False, True)
        img = pygame.transform.scale2x(img)
        window.blit(img, (0, 0))
        
        lidar = pygame.surfarray.make_surface(gray(graphics[:,:,3]))
        lidar = pygame.transform.rotate(lidar, 90)
        lidar = pygame.transform.flip(lidar, False, True)
        lidar = pygame.transform.scale2x(lidar)
        window.blit(lidar, (0, 144*2))

        font = pygame.font.SysFont('ubuntumono', 20)
        text = font.render('speed: {:.1f}m/s'.format(speed), True, (0, 255, 64))
        window.blit(text, (5, 5))
        text = font.render('rpm: {:.1f}'.format(rpm), True, (0, 255, 64))
        window.blit(text, (5, 25))
        text = font.render('gear: {}'.format(int(gear)), True, (0, 255, 64))
        window.blit(text, (5, 45))
        text = font.render('collision: {}'.format('false' if (not collision) else 'true'), True, (0, 255, 64))
        window.blit(text, (5, 65))

        pygame.display.update()

        return accel,breaking,steering
    
    def initAirsim(self):
        self.airsim_client = airsim.CarClient(ip = "192.168.1.171")
        self.airsim_client.confirmConnection()
        self.airsim_client.enableApiControl(True)
        self.airsim_client.reset()
        self.airsim_client.simPause(False)
        self.airsim_car = airsim.CarControls()
        

    def generate(self,duration,display_duration=False,state0_graphics=None,state0_sensors=None):

        if self.target == 'airsim':

            if self.display_current_stat:
                pygame.init()
                window = pygame.display.set_mode((256*2, 144*4), 0, 32)
                window.fill((0,0,0))

            self.clear()

            self.initAirsim()

            stats_graphics = []
            stats_sensors = []
            actions = []
            sim_time = []
            
            t0,tcurrent = 0,0

            stop = False
            while (not stop) and ((tcurrent-t0)<duration):

                # get state of the car
                car_state = self.airsim_client.getCarState()

                sensors=np.array([car_state.speed, self.airsim_client.simGetCollisionInfo().has_collided, car_state.rpm, car_state.gear])

                if self.dt is None:
                    if t0 == 0:
                        t0 = car_state.timestamp/1e9
                    tcurrent = car_state.timestamp/1e9
                
                #stop condition (collide)
                stop = sensors[1]
                
                #generate input
                graphics=self.generateInput()

                #generate action
                self.airsim_car.throttle,self.airsim_car.breaking,self.airsim_car.steering = self.getAction(sensors)

                #controle car
                self.airsim_client.setCarControls(self.airsim_car)

                #display current state
                if self.display_current_stat:
                    self.display_stat(pygame,window,car_state.speed, self.airsim_client.simGetCollisionInfo().has_collided, car_state.rpm, car_state.gear ,graphics=graphics)

                if display_duration:
                    print('current time: {:.2f}'.format(tcurrent-t0))

                #update silutation with constante step (if dt is set)
                if self.dt is not None:
                    self.airsim_client.simContinueForTime(self.dt)

                #update stats and actions for replay
                sim_time.append(tcurrent - t0)
                stats_graphics.append(graphics)
                stats_sensors.append(sensors)
                actions.append([self.airsim_car.throttle,self.airsim_car.breaking,self.airsim_car.steering])

                if self.dt is not None:
                    tcurrent += self.dt
            
            #convert to numpy array
            self.stats_graphics=np.array(stats_graphics)
            self.stats_sensors=np.array(stats_sensors)
            self.actions=np.array(actions)
            self.time=np.array(sim_time)

            self.airsim_client.reset()

            if self.display_current_stat:
                pygame.quit()

            return tcurrent-t0
        elif self.target == 'model':

            if self.dt is None:
                print('error: dt is needed in word model')
                exit(-1)

            if self.display_current_stat:
                pygame.init()
                window = pygame.display.set_mode((256*2, 144*4), 0, 32)
                window.fill((0,0,0))

            self.clear()

            vectorized_graphics = self.model.model_encoder.predict([[state0_graphics[:,:,:3]],[state0_graphics[:,:,3]]])
            stats_vectorized_graphics = [vectorized_graphics[0]]
            stats_sensors = [state0_sensors]
            actions = [np.zeros(3)]
            sim_time = [np.zeros(1)]
            
            t0,tcurrent = 0,0.1

            stop = False
            while (not stop) and ((tcurrent-t0)<duration):
                # get state of the car
                sensors=stats_sensors[-1]
                
                #stop condition (collide)
                #stop = sensors[1]

                #generate action
                throttle,breaking,steering = self.getAction(sensors)

                #update using word model
                norm_vec_graphics = self.model.normalize_vectorized_graphics(stats_vectorized_graphics[-1])
                norm_sensors = self.model.normalize_sensors(stats_sensors[-1])
                print(stats_vectorized_graphics[-1])
                #norm_next_sensors = [norm_sensors]
                #norm_next_vec_graphics = [norm_vec_graphics]
                [norm_next_vec_graphics,norm_next_sensors] = self.model.model_transfer_function.predict([np.array([norm_vec_graphics]),
                                                                                                        np.array([norm_sensors[0]]),
                                                                                                        np.array([norm_sensors[1]]),
                                                                                                        np.array([norm_sensors[2]]),
                                                                                                        np.array([norm_sensors[3]]),
                                                                                                        np.array([[throttle,breaking,steering]])])
                
                graphics = self.model.denormalize_vectorized_graphics(norm_next_vec_graphics[0])
                sensors = self.model.denormalize_sensors(norm_next_sensors[0])

                #display current state
                if self.display_current_stat:
                    '''
                    self.display_stat(pygame,window,sensors[0], sensors[1], sensors[2], sensors[3] ,
                                        graphics=self.model.model_decoder.predict(np.array([graphics]))[0])
                    '''
                    self.display_stat(pygame,window,sensors[0], sensors[1], sensors[2], sensors[3] ,
                                        graphics=np.clip(self.model.model_decoder.predict(np.array([graphics]))[0]*255,0,255))

                if display_duration:
                    print('current time: {:.2f}'.format(tcurrent-t0))

                #update stats and actions for replay
                sim_time.append(tcurrent - t0)
                stats_vectorized_graphics.append(graphics)
                stats_sensors.append(sensors)
                actions.append([throttle,breaking,steering])
                print([throttle,breaking,steering])

                time.sleep(self.dt)
                if self.dt is not None:
                    tcurrent += self.dt
            
            #convert to numpy array
            self.stats_vectorized_graphics=np.array(stats_vectorized_graphics)
            self.stats_sensors=np.array(stats_sensors)
            self.actions=np.array(actions)
            self.time=np.array(sim_time)

            if self.display_current_stat:
                pygame.quit()

            return tcurrent-t0
        else:
            print('error: episode generation target not found')
            exit(-1)
        

