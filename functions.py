import model
from episode import *
from dataset import *
import numpy as np
import matplotlib.pyplot as plt
import argparse
import pandas as pd
import json
import gc

class NumpyEncoder(json.JSONEncoder):
    ''' Special json encoder for numpy types '''
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
            np.int16, np.int32, np.int64, np.uint8,
            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32, 
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)): #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def episode_generate(name,input_type='keyboard',duration=60):
    episode = Episode(target='airsim',action=input_type,display_current_stat=True,dt=0.1)
    d = episode.generate(duration,display_duration=True)
    print('simulation duration: {:.2f}'.format(d))
    episode.save(name)

def episode_generate_wordmodel(name,episode_state0,model_directory,input_type='keyboard',duration=60):
    deep_model = model.DeepTakumi()
    deep_model.load(model_directory,
                autoencoder_model=True,autoencoder_weights=True,
                transfer_function_model=True,transfer_function_weights=True,
                q_model=False,q_weights=False,
                normalization_parameter=True)
    episode = Episode(target='model',action=input_type,model=deep_model,display_current_stat=True,dt=0.1)

    episode0 = Episode()
    episode0.load(episode_state0)

    d = episode.generate(duration,display_duration=True,state0_graphics=episode0.stats_graphics[0]/255,state0_sensors=episode0.stats_sensors[0])
    print('simulation duration: {:.2f}'.format(d))
    episode.save(name)

def episode_watch(name):
    episode = Episode(display_current_stat=True)
    episode.load(name)
    episode.display_replay()

def episode_vectorize(name,model_name,vectorize=True):
    from keras import backend as K 
    deep_model = model.DeepTakumi()
    deep_model.load(model_directory=model_name,
                autoencoder_model=True,autoencoder_weights=True,
                transfer_function_model=False,transfer_function_weights=False,
                q_model=False,q_weights=False,
                normalization_parameter=False)
    
    for i,e_name in enumerate(name):
        print('episode {}/{}'.format(i+1,len(name)))
        episode = Episode(model=deep_model,display_current_stat=True)
        episode.load(e_name)

        if vectorize:
            episode.vectorize()
        else:
            episode.unvectorize()
        
        episode.save(e_name)

    del deep_model
    K.clear_session()

def dataset_create_from_episodes(names,count=100,output_dir='tmp/dataset'):
    dataset = DatasetTransferFunction()
    dataset.generateFromEpisodes(names,count,log=True)
    dataset.save(output_dir)

def model_build_config(args):
    config = {'autoencoder':{
                'lr':args.autoenc_lr,
                'loss':args.autoenc_loss,
                'epochs':args.autoenc_epochs,
                'batch_size':args.autoenc_batch_size,
                'layers': args.autoenc_layers
                },
            'transfer_function':{
                'lr':args.transfer_lr,
                'loss':args.transfer_loss,
                'epochs':args.transfer_epochs,
                'batch_size':args.transfer_batch_size,
                'layers': args.transfer_layers
                },
            'q_function':{
                'lr':args.q_lr,
                'loss':args.q_loss,
                'epochs':args.q_epochs,
                'batch_size':args.q_batch_size,
                'layers': args.q_layers
                }
            }
    return config

def model_display_autoencoder(deep_model,stats_graphics):
    N=6
    for i,j in enumerate(np.random.choice(stats_graphics.shape[0],N)):
        plt.subplot(N,4,i*4+1)
        plt.imshow(stats_graphics[j,:,:,:3])
        plt.subplot(N,4,i*4+2)
        plt.imshow(stats_graphics[j,:,:,3])

        cam = np.array([stats_graphics[j,:,:,:3]])
        lidar = np.array([stats_graphics[j,:,:,3]])

        predicted_stats_graphics = deep_model.model_autoencoder.predict(x=[cam,lidar])

        plt.subplot(N,4,i*4+3)
        plt.imshow(np.clip(predicted_stats_graphics[0,:,:,:3],0,1))
        plt.subplot(N,4,i*4+4)
        plt.imshow(np.clip(predicted_stats_graphics[0,:,:,3],0,1))
    plt.show()

def model_train_autoencoder(episodes_name=None,graphics=None,directory=None,config={},log=None,display_result=False):
    from keras import backend as K 
    deep_model = model.DeepTakumi(config=config)

    deep_model.build()

    #load data
    print('load data')
    stats_graphics = []
    if graphics is None:
        for name in episodes_name:
            episode = Episode()
            episode.load(name)
            stats_graphics.append(episode.stats_graphics/255)
        stats_graphics = np.concatenate(stats_graphics)
    else:
        stats_graphics = graphics

    #train
    print('train model')
    history = deep_model.model_autoencoder.fit(x=[stats_graphics[:,:,:,:3],stats_graphics[:,:,:,3]],
                                                y=stats_graphics,
                                                batch_size=deep_model.config['autoencoder']['batch_size'],
                                                epochs=deep_model.config['autoencoder']['epochs'])

    #save result
    if directory is not None:
        deep_model.save(model_directory=directory,autoencoder_model=True,autoencoder_weights=True,
            transfer_function_model=False,transfer_function_weights=False,
            q_model=False,q_weights=False,
            normalization_parameter=False)
    #display result
    if display_result:
        model_display_autoencoder(deep_model,stats_graphics)
    if log is not None:
        df = pd.DataFrame(np.transpose([history.history['loss'],history.history['acc']]), columns=['loss','accuracy'])
        df.to_csv('{}/{}'.format(directory,log))
    
    del deep_model
    K.clear_session()
    return history.history['loss'][-1],history.history['acc'][-1]

def model_train_transfer_function(dataset_name=None,graphics=None,directory=None,config={},log=None,display_result=False):
    from keras import backend as K 
    deep_model = model.DeepTakumi(config=config)

    deep_model.build()

    #load data
    print('load data')
    dataset = DatasetTransferFunction(dataset_name)

    #train
    print('train model')
    history = deep_model.model_transfer_function.fit(x=dataset.getX(),
                                                y=dataset.getY(),
                                                batch_size=deep_model.config['transfer_function']['batch_size'],
                                                epochs=deep_model.config['transfer_function']['epochs'])

    deep_model.normalization_parameter={'vecorized_graphics':{
                                            'mean':float(dataset.graphics_mean),
                                            'std':float(dataset.graphics_std)},
                                        'sensors':{
                                            'mean':[float(dataset.sensors_mean[i]) for i in range(4)],
                                            'std':[float(dataset.sensors_std[i]) for i in range(4)]
                                            }
                                        }
    #save result
    if directory is not None:
        deep_model.save(model_directory=directory,autoencoder_model=False,autoencoder_weights=False,
            transfer_function_model=True,transfer_function_weights=True,
            q_model=False,q_weights=False,
            normalization_parameter=True)
    #display result
    # TODO
    #if display_result:
    #    model_display_autoencoder(deep_model,stats_graphics)
    
    if log is not None:
        df = pd.DataFrame(np.transpose([history.history['loss']]), columns=['loss'])
        df.to_csv('{}/{}'.format(directory,log))
    
    del deep_model
    K.clear_session()
    #return history.history['loss'][-1],history.history['acc'][-1]
        
def model_test(episodes_name,model_type='autoencoder',directory='model'):
    from keras import backend as K 
    deep_model = model.DeepTakumi()

    if model_type == 'autoencoder':
        deep_model.load(model_directory=directory,
                autoencoder_model=True,autoencoder_weights=True,
                transfer_function_model=False,transfer_function_weights=False,
                q_model=False,q_weights=False,
                normalization_parameter=False)

        #load data
        print('load data')
        stats_graphics = []

        for name in episodes_name:
            episode = Episode()
            episode.load(name)
            stats_graphics.append(episode.stats_graphics/255)
        stats_graphics = np.concatenate(stats_graphics)
        
        #display result
        model_display_autoencoder(deep_model,stats_graphics)

    del deep_model
    K.clear_session()

def mult(arrayA,arrayB):
    lst=[]
    if len(arrayA) > 0 and len(arrayB) > 0:
        for a in arrayA:
            for b in arrayB:
                if type(a) == list and type(b) == list:
                    lst.append(a+b)
                elif type(a) == list:
                    lst.append(a+[b])
                elif type(b) == list:
                    lst.append([a]+b)
                elif type(a) == dict and type(b) == dict:
                    lst.append({**a,**b})
                else:
                    lst.append([a,b])
        return lst
    elif len(arrayA) > 0:
        return arrayA
    return arrayB

def mult_arrays(d,array_lst=[]):
    for value in d:
        array_lst=mult(array_lst,value)
    return array_lst

def model_load_configs(file_name):
    with open(file_name, 'r') as f:
        configs = json.loads(f.read())
        configs['autoencoder']['layers']=[np.array(l) for l in configs['autoencoder']['layers']]
        configs['transfer_function']['layers']=[np.array(l) for l in configs['transfer_function']['layers']]
        configs['q_function']['layers']=[np.array(l) for l in configs['q_function']['layers']]
        return configs
    return []

def model_generate_grid(configs):
    autoenc_config_list = []
    res=mult_arrays(configs['autoencoder'].values())
    for c in res:
        autoenc_config_list.append({'autoencoder':dict(zip(configs['autoencoder'].keys(), c))})

    transfer_config_list = []
    res=mult_arrays(configs['transfer_function'].values())
    for c in res:
        transfer_config_list.append({'transfer_function':dict(zip(configs['transfer_function'].keys(), c))})

    q_config_list = []
    res=mult_arrays(configs['q_function'].values())
    for c in res:
        q_config_list.append({'q_function':dict(zip(configs['q_function'].keys(), c))})
    
    config_list=mult_arrays([autoenc_config_list,transfer_config_list,q_config_list])

    return config_list

def model_train_grid(episodes_name,model_type='autoencoder',directory=None,configs={},log=None,display_result=False):
    config_list = model_generate_grid(configs)
    results = np.zeros((len(config_list),3))

    print('load data')
    stats_graphics = []

    for name in episodes_name:
        episode = Episode()
        episode.load(name)
        stats_graphics.append(episode.stats_graphics/255)
    stats_graphics = np.concatenate(stats_graphics)
    
    for i,c in enumerate(config_list):
        print('{:*^128}'.format(' TRAIN {}-{} '.format(directory,i)))
        print(c)
        results[i,0] = i
        results[i,1],results[i,2] = model_train_autoencoder(graphics=stats_graphics,directory='{}-{}'.format(directory,i),config=c,log=log,display_result=display_result)
        with open('{}-{}/config.json'.format(directory,i), 'w+') as fp:
            dumped = json.dumps(c, sort_keys=True, indent=4, cls=NumpyEncoder)
            fp.write(dumped)
        
        gc.collect()
        time.sleep(1)
    
    results = [results[i,:] for i,_ in enumerate(config_list)]
    results = sorted(results, key=lambda x: x[2], reverse = True)
    print('results:')
    print('{: ^6}|{: ^10}|{: ^10}|{: ^128}'.format('id','loss','accuracy','parameter'))
    print('{:-^6}+{:-^10}+{:-^10}+{:-^128}'.format('','','',''))
    for i,_ in enumerate(config_list):
        parameters = json.dumps(config_list[int(results[i][0])]['autoencoder'], cls=NumpyEncoder)
        print('{: >6.0f}|{: ^10.5f}|{: ^10.5f}|{: ^128}'.format(results[i][0],results[i][1],results[i][2],parameters))
