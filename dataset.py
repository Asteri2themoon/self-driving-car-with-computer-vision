import pandas as pd
import numpy as np
import os

class DatasetTransferFunction:
    def __init__(self,name=None):
        self.state_sensors_input = np.array([])
        self.state_vectorized_graphics_input = np.array([])
        self.actions = np.array([])
        self.state_sensors_output = np.array([])
        self.state_vectorized_graphics_output = np.array([])
        self.graphics_mean = 0
        self.graphics_std = 1
        self.sensors_mean = np.zeros(4)
        self.sensors_std = np.ones(4)

        if name is not None:
            self.load(name)

    def load(self,name):
        tmp=np.load('{}/state_sensors_input.npz'.format(name))
        self.state_sensors_input=tmp.f.A[0]
        tmp=np.load('{}/state_vectorized_graphics_input.npz'.format(name))
        self.state_vectorized_graphics_input=tmp.f.A[0]
        tmp=np.load('{}/actions.npz'.format(name))
        self.actions=tmp.f.A[0]
        tmp=np.load('{}/state_sensors_output.npz'.format(name))
        self.state_sensors_output=tmp.f.A[0]
        tmp=np.load('{}/state_vectorized_graphics_output.npz'.format(name))
        self.state_vectorized_graphics_output=tmp.f.A[0]

        self.graphics_mean = np.mean([self.state_vectorized_graphics_input,self.state_vectorized_graphics_output])
        self.graphics_std = np.std([self.state_vectorized_graphics_input,self.state_vectorized_graphics_output])

        print('graphics: ',self.graphics_mean,self.graphics_std)
        
        self.state_vectorized_graphics_input = (self.state_vectorized_graphics_input-self.graphics_mean)/self.graphics_std
        self.state_vectorized_graphics_output = (self.state_vectorized_graphics_output-self.graphics_mean)/self.graphics_std

        self.sensors_mean = np.mean(self.state_sensors_input,axis=0)
        self.sensors_std = np.std(self.state_sensors_input,axis=0)

        print('sensors: ',self.sensors_mean,self.sensors_std)
        
        print(self.state_sensors_input[:,0])
        self.state_sensors_input[:,0] = (self.state_sensors_input[:,0]-self.sensors_mean[0])/self.sensors_std[0]
        self.state_sensors_output[:,0] = (self.state_sensors_output[:,0]-self.sensors_mean[0])/self.sensors_std[0]
        
        self.state_sensors_input[:,2] = (self.state_sensors_input[:,2]-self.sensors_mean[2])/self.sensors_std[2]
        self.state_sensors_output[:,2] = (self.state_sensors_output[:,2]-self.sensors_mean[2])/self.sensors_std[2]
        
        self.state_sensors_input[:,3] = (self.state_sensors_input[:,3]-self.sensors_mean[3])/self.sensors_std[3]
        self.state_sensors_output[:,3] = (self.state_sensors_output[:,3]-self.sensors_mean[3])/self.sensors_std[3]

        print(self.state_sensors_input.shape)
        print(self.state_vectorized_graphics_input.shape)
        print(self.actions.shape)
        print(self.state_sensors_output.shape)
        print(self.state_vectorized_graphics_output.shape)

    def save(self,name):
        if not os.path.exists(name):
            os.makedirs(name)

        np.savez('{}/state_sensors_input.npz'.format(name),A=np.array(self.state_sensors_input))
        np.savez('{}/state_vectorized_graphics_input.npz'.format(name),A=np.array(self.state_vectorized_graphics_input))
        np.savez('{}/actions.npz'.format(name),A=np.array(self.actions))
        np.savez('{}/state_sensors_output.npz'.format(name),A=np.array(self.state_sensors_output))
        np.savez('{}/state_vectorized_graphics_output.npz'.format(name),A=np.array(self.state_vectorized_graphics_output))
    
    def generateFromEpisodes(self,names,count,log=False):
        import episode

        lengths = np.cumsum([episode.Episode.loadSize(name_episode)-1 for name_episode in names])
        if lengths[-1] < count:
            print('error: try to generate {} samples from {} frames'.format(count,lengths[-1]))
            exit(-1)

        if log:
            print('transition count: {}'.format(lengths[-1]))

        random_sample = np.random.choice(range(lengths[-1]), count, replace=False)
        random_sample.sort()
        
        def getId(i):
            for j,l in enumerate(lengths):
                if l > i:
                    return j, i-l
            print('error')
            return 0,0

        random_sample = [getId(i) for i in random_sample]

        state_sensors_input = []
        state_vectorized_graphics_input = []
        actions = []
        state_sensors_output = []
        state_vectorized_graphics_output = []

        prev = -1
        loaded_episode = None
        for epId,frameId in random_sample:
            if prev != epId:
                if log:
                    print('load episode {}'.format(epId))
                loaded_episode = episode.Episode()
                loaded_episode.load(names[epId])
                prev = epId
            
            state_sensors_input.append(loaded_episode.stats_sensors[frameId,:])
            state_vectorized_graphics_input.append(loaded_episode.stats_vectorized_graphics[frameId,:,:,:])
            actions.append(loaded_episode.actions[frameId,:])
            state_sensors_output.append(loaded_episode.stats_sensors[frameId+1,:])
            state_vectorized_graphics_output.append(loaded_episode.stats_vectorized_graphics[frameId+1,:,:,:])

        if log:
            print('save data')

        self.state_sensors_input = np.array([state_sensors_input])
        self.state_vectorized_graphics_input = np.array([state_vectorized_graphics_input])
        self.actions = np.array([actions])
        self.state_sensors_output = np.array([state_sensors_output])
        self.state_vectorized_graphics_output = np.array([state_vectorized_graphics_output])

    def getX(self):
        return [self.state_vectorized_graphics_input,
                self.state_sensors_input[:,0]/10,self.state_sensors_input[:,1],self.state_sensors_input[:,2]/1000,self.state_sensors_input[:,3],
                self.actions]

    def getY(self):
        return [self.state_vectorized_graphics_output,self.state_sensors_output]
    
    def getTrainingSetSize(self):
        return self.state_sensors_input.shape[0]
