from functions import *
from model import *
import argparse
import json
import glob

def glob_all(lst_pattern):
    files = []
    for pattern in lst_pattern:
        files.extend(glob.glob(pattern))
    return files

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # ******************************** episode ********************************

    episode = subparsers.add_parser('episode', help='episode command')
    episode.set_defaults(module='episode')

    # generate
    episode_function = episode.add_subparsers()
    episode_function_generate = episode_function.add_parser('generate', help='generate episode')
    episode_function_generate.set_defaults(function='generate')

    episode_function_generate.add_argument('name', nargs='+', help='name of the episodes')
    episode_function_generate.add_argument('-input', nargs='?', help='input type', default='keyboard')
    episode_function_generate.add_argument('-duration', nargs='?', help='episode duration', default=60, type=int)
    episode_function_generate.add_argument('-episode-state0', nargs='?', help='first state of the simulation (only on word model)')
    episode_function_generate.add_argument('-model', nargs='?', help='word model\'s directory (only on word model)',default=None)

    # watch
    episode_function_watch = episode_function.add_parser('watch', help='watch episode')
    episode_function_watch.set_defaults(function='watch')

    episode_function_watch.add_argument('name', nargs=1, help='name of the episode')

    # vectorize
    episode_function_watch = episode_function.add_parser('vectorize', help='vectorize episode')
    episode_function_watch.set_defaults(function='vectorize')

    episode_function_watch.add_argument('names', nargs='+', help='name of the episodes')
    episode_function_watch.add_argument('-directory', nargs=1, help='name of the model')

    # unvectorize
    episode_function_watch = episode_function.add_parser('unvectorize', help='unvectorize episode')
    episode_function_watch.set_defaults(function='unvectorize')

    episode_function_watch.add_argument('names', nargs='+', help='name of the episodes')
    episode_function_watch.add_argument('-directory', nargs=1, help='name of the model')

    # ******************************** dataset ********************************

    dataset = subparsers.add_parser('dataset', help='dataset command')
    dataset.set_defaults(module='dataset')

    # create from episode
    dataset_function = dataset.add_subparsers()
    dataset_function_create = dataset_function.add_parser('create', help='generate episode from episode')
    dataset_function_create.set_defaults(function='create')

    dataset_function_create.add_argument('names', nargs='+', help='name of the episodes')
    dataset_function_create.add_argument('-count', nargs='?', help='random sample count',type=int,default=100)
    dataset_function_create.add_argument('-type', nargs='?', help='type of dataset (transfer_function/q_learning)',default='transfer_function')
    dataset_function_create.add_argument('-output', nargs='?', help='output directory',default='tmp/dataset')

    # ******************************** model ********************************

    model = subparsers.add_parser('model', help='model command')
    model.set_defaults(module='model')
    model_action = model.add_subparsers()

    # train
    model_action_train = model_action.add_parser('train', help='generate default configuration file')
    model_action_train.set_defaults(function='train')
    model_action_train.add_argument('data', nargs='+', help='episodes or dataset used for training')
    model_action_train.add_argument('-model', nargs=1, help='model to train', default='autoencoder')

    model_action_train.add_argument('-directory', nargs='?', help='custom save directory',default=None)
    model_action_train.add_argument('-display-result', action='store_true',help='display benchmark at the end')
    model_action_train.add_argument('-log', nargs='?', help='log file name function',default=None)

    model_action_train.add_argument('-autoenc-lr', nargs='?', help='learning rate', default=DeepTakumi.default_config['autoencoder']['lr'], type=float)
    model_action_train.add_argument('-autoenc-loss', nargs='?', help='loss function', default=DeepTakumi.default_config['autoencoder']['loss'])
    model_action_train.add_argument('-autoenc-epochs', nargs='?', help='epochs',default=DeepTakumi.default_config['autoencoder']['epochs'],type=int)
    model_action_train.add_argument('-autoenc-batch-size', nargs='?', help='batch size',default=DeepTakumi.default_config['autoencoder']['batch_size'],type=int)
    model_action_train.add_argument('-autoenc-layers', nargs='?', help='batch size',default=DeepTakumi.default_config['autoencoder']['layers'],type=list)

    model_action_train.add_argument('-transfer-lr', nargs='?', help='learning rate', default=DeepTakumi.default_config['transfer_function']['lr'], type=float)
    model_action_train.add_argument('-transfer-loss', nargs='?', help='loss function', default=DeepTakumi.default_config['transfer_function']['loss'])
    model_action_train.add_argument('-transfer-epochs', nargs='?', help='epochs',default=DeepTakumi.default_config['transfer_function']['epochs'],type=int)
    model_action_train.add_argument('-transfer-batch-size', nargs='?', help='batch size',default=DeepTakumi.default_config['transfer_function']['batch_size'],type=int)
    model_action_train.add_argument('-transfer-layers', nargs='?', help='batch size',default=DeepTakumi.default_config['transfer_function']['layers'],type=list)

    model_action_train.add_argument('-q-lr', nargs='?', help='learning rate', default=DeepTakumi.default_config['q_function']['lr'], type=float)
    model_action_train.add_argument('-q-loss', nargs='?', help='loss function', default=DeepTakumi.default_config['q_function']['loss'])
    model_action_train.add_argument('-q-epochs', nargs='?', help='epochs',default=DeepTakumi.default_config['q_function']['epochs'],type=int)
    model_action_train.add_argument('-q-batch-size', nargs='?', help='batch size',default=DeepTakumi.default_config['q_function']['batch_size'],type=int)
    model_action_train.add_argument('-q-layers', nargs='?', help='batch size',default=DeepTakumi.default_config['q_function']['layers'],type=list)

    # test
    model_action_train = model_action.add_parser('test', help='generate default configuration file')
    model_action_train.set_defaults(function='test')
    model_action_train.add_argument('-config_name', nargs='?', help='name of the configuration file', default='config.json')
    model_action_train.add_argument('-directory', nargs='?', help='custom save directory',default='model')
    model_action_train.add_argument('-model', nargs=1, help='model to train', default='autoencoder')
    model_action_train.add_argument('episodes', nargs='+', help='episodes used for training')

    # grid
    model_action_grid = model_action.add_parser('grid', help='grid search')
    model_action_grid.set_defaults(function='grid')

    model_action_grid.add_argument('episodes', nargs='+', help='episodes used for training')
    model_action_grid.add_argument('-model', nargs=1, help='model to train', default='autoencoder')

    model_action_grid.add_argument('-directory', nargs='?', help='custom save directory',default=None)
    model_action_grid.add_argument('-display-result', action='store_true',help='display benchmark at the end')
    model_action_grid.add_argument('-log', nargs='?', help='log file name function',default=None)

    model_action_grid.add_argument('-configs', nargs=1, help='configs file')

    args = parser.parse_args()
    print(args)

    if args.module == 'episode':
        if args.function == 'generate':
            if args.model is not None:
                episode_generate_wordmodel(args.name[0],episode_state0=args.episode_state0,model_directory=args.model,input_type=args.input,duration=args.duration)
            else:
                episode_generate(args.name[0],input_type=args.input,duration=args.duration)
        elif args.function == 'watch':
            episode_watch(args.name[0])
        elif args.function == 'vectorize':
            episode_vectorize(glob_all(args.names),args.directory[0],vectorize=True)
        elif args.function == 'unvectorize':
            episode_vectorize(glob_all(args.names),args.directory[0],vectorize=False)
        else:
            print('error: function not found')
    elif args.module == 'dataset':
        if args.function == 'create':
            if args.type == 'transfer_function':
                dataset_create_from_episodes(glob_all(args.names),args.count,output_dir='tmp/dataset')
            else:
                print('error: dataset type not found')
        else:
            print('error: function not found')
    elif args.module == 'model':
        if args.function == 'train':
            config = model_build_config(args)
            if args.model == 'autoencoder':
                model_train_autoencoder(episodes_name=glob_all(args.data),directory=args.directory,config=config,
                                        log=args.log,display_result=args.display_result)
            elif args.model[0] == 'transfer_function':
                model_train_transfer_function(dataset_name=args.data[0],directory=args.directory,config=config,
                                        log=args.log,display_result=args.display_result)
            else:
                print('error: model type not found')
        elif args.function == 'test':
            model_test(episodes_name=glob_all(args.episodes),model_type=args.model[0],directory=args.directory)
        elif args.function == 'grid':
            configs = model_load_configs(args.configs[0])
            model_train_grid(episodes_name=glob_all(args.episodes),model_type=args.model[0],directory=args.directory,
                        configs=configs,
                        log=args.log,display_result=args.display_result)
        else:
            print('error: function not found')
    else:
        print('error: module not found')


