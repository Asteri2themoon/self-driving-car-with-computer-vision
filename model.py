'''
import keras
from keras.layers import Input, Conv2D, Dense, MaxPooling2D, UpSampling2D, Reshape, Flatten, Conv2DTranspose
from keras.models import Model, model_from_json
from keras import backend as K
from keras import optimizers
'''
import numpy as np

import json
import os

class DeepTakumi:
    constant_config = {'image':{
                            'width':256,
                            'height':144
                        },
                        'normalization_parameters': 'normalize.json',
                        'autoencoder':{
                            'model':'autoencoder_models.json',
                            'weights':'autoencoder_weights.h5'
                        },'encoder':{
                            'model':'encoder_models.json',
                            'weights':'encoder_weights.h5'
                        },'decoder':{
                            'model':'decoder_models.json',
                            'weights':'decoder_weights.h5'
                        },'transfer_function':{
                            'model':'transfer_function_models.json',
                            'weights':'transfer_function_weights.h5'
                        },'q_function':{
                            'model':'q_function_models.json',
                            'weights':'q_function_weights.h5'
                        }
                    }
    default_config = {'autoencoder':{
                            'layers': [8,8],
                            'loss':'mse',
                            'lr':0.01,
                            'batch_size':32,
                            'epochs':10
                            },
                        'transfer_function':{
                            'layers': [4096,1024,4096],
                            'loss':'mse',
                            'lr':0.0001,
                            'batch_size':32,
                            'epochs':20
                            },
                        'q_function':{
                            'layers': [4096,1024,4096],
                            'loss':'mse',
                            'lr':0.01,
                            'batch_size':32,
                            'epochs':20
                            }
                        }

    keras = None

    def __init__(self,config={},log=True):
        if self.keras is None:# tricks to load keras dynamiclly when needed
            self.keras = __import__('keras')

        self.config = {**self.default_config,**config}
        self.log = log

        self.normalization_parameter = {'vecorized_graphics':{
                                            'mean':0,
                                            'std':1},
                                        'sensors':{
                                            'mean':[0,0,0,0],
                                            'std':[1,1,1,1]
                                            }
                                        }

    def build(self):
        #input defition

        self.camera = self.keras.layers.Input(shape=(self.constant_config['image']['height'],self.constant_config['image']['width'],3),dtype=np.float32)
        self.lidar = self.keras.layers.Input(shape=(self.constant_config['image']['height'],self.constant_config['image']['width']),dtype=np.float32)

        self.speed = self.keras.layers.Input(shape=(1,),dtype=np.float32)
        self.collision = self.keras.layers.Input(shape=(1,),dtype=np.float32)
        self.rpm = self.keras.layers.Input(shape=(1,),dtype=np.float32)
        self.gear = self.keras.layers.Input(shape=(1,),dtype=np.float32)
        self.actions = self.keras.layers.Input(shape=(3,),dtype=np.float32)

        merged_vision = self.keras.layers.concatenate([self.camera, self.keras.layers.Reshape((self.constant_config['image']['height'],self.constant_config['image']['width'],1))(self.lidar)], axis=-1)

        #build autoencoder

        #encoding
        encoder=merged_vision
        for filters in self.config['autoencoder']['layers']:
            encoder = self.keras.layers.Conv2D(filters,padding='same',kernel_size=(3,3),data_format='channels_last')(encoder)
            encoder = self.keras.layers.MaxPooling2D(pool_size=(2, 2),data_format='channels_last')(encoder)
        self.model_encoder=self.keras.models.Model(inputs=[self.camera,self.lidar], outputs=encoder)
        
        #decoding
        decoder = encoder
        for filters in self.config['autoencoder']['layers'][-2::-1]:
            decoder = self.keras.layers.Conv2DTranspose(filters,padding="same",kernel_size=(3,3),strides=2,data_format="channels_last")(decoder)
        decoder = self.keras.layers.Conv2DTranspose(4,padding="same",kernel_size=(3,3),strides=2,data_format="channels_last")(decoder)
        self.model_autoencoder=self.keras.models.Model(inputs=[self.camera,self.lidar], outputs=decoder)

        #standalone decoder
        _,w,h,k=self.model_autoencoder.layers[-len(self.config['autoencoder']['layers'])].input_shape
        encoded_input = self.keras.layers.Input(shape=(w,h,k))
        decoder = encoded_input
        for l in self.model_autoencoder.layers[-len(self.config['autoencoder']['layers']):]:
            decoder = l(decoder)
        self.model_decoder=self.keras.models.Model(inputs=encoded_input, outputs=decoder)

        #build transfer fuction

        _,w,h,z=encoder.shape
        w,h,z=int(w),int(h),int(z)
        print(w,h,z)
        self.vectorized_image = self.keras.layers.Input(shape=(w,h,z),dtype=np.float32)
        self.normalize_vectorized_image = self.keras.layers.BatchNormalization()(self.vectorized_image)
        self.normalize_vectorized_image = self.keras.layers.Dropout(0.2)(self.normalize_vectorized_image)
        merged_state = self.keras.layers.concatenate([self.speed, self.collision, self.rpm, self.gear, self.keras.layers.Flatten()(self.normalize_vectorized_image)], axis=-1)

        
        self.tranfer_function = self.keras.layers.concatenate([merged_state, self.actions], axis=-1)
        for l in self.config['transfer_function']['layers']:
            self.tranfer_function = self.keras.layers.Dense(l,activation="relu")(self.tranfer_function)
        
        self.transfer_function_image = self.keras.layers.Dense(w*h*z)(self.tranfer_function)
        self.transfer_function_image = self.keras.layers.Reshape((w,h,z))(self.transfer_function_image)

        self.transfer_function_sensors = self.keras.layers.Dense(4)(self.tranfer_function)

        self.model_transfer_function=self.keras.models.Model(inputs=[self.vectorized_image,self.speed,self.collision, self.rpm, self.gear,self.actions], outputs=[self.transfer_function_image,self.transfer_function_sensors])
        
        #build Q network
        
        self.Q = merged_state
        for l in self.config['q_function']['layers']:
            self.Q = self.keras.layers.Dense(l,activation="relu")(self.Q)
        self.Q = self.keras.layers.Dense(7,activation="relu")(self.Q)

        self.model_q = self.keras.models.Model(inputs=[self.vectorized_image,self.speed,self.collision, self.rpm, self.gear],outputs=self.Q)

        # discretised action state
        # 1 output for accelerating action's value
        # 1 output for breaking action's value
        # 5 outputs for steering action's value (-1,-0.5,0,0.5,1)

        #build model
        self.compile()

        if self.log:
            print('autoencoder:')
            print(self.model_autoencoder.summary())

            print('encoder:')
            print(self.model_encoder.summary())

            print('decoder:')
            print(self.model_decoder.summary())
            
            print('transfer function:')
            print(self.model_transfer_function.summary())

            print('Q network:')
            print(self.model_q.summary())
            
    def normalize_vectorized_graphics(self,vectorized_graphics):
        mean = self.normalization_parameter['vecorized_graphics']['mean']
        std = self.normalization_parameter['vecorized_graphics']['std']
        return (vectorized_graphics-mean)/std
            
    def normalize_sensors(self,sensors):
        mean = self.normalization_parameter['sensors']['mean']
        std = self.normalization_parameter['sensors']['std']

        normalized_sensors = sensors.copy()
        
        normalized_sensors[0] = (normalized_sensors[0]-mean[0])/std[0]
        normalized_sensors[2] = (normalized_sensors[2]-mean[2])/std[2]
        normalized_sensors[3] = (normalized_sensors[3]-mean[3])/std[3]

        return normalized_sensors
            
    def denormalize_vectorized_graphics(self,vectorized_graphics):
        mean = self.normalization_parameter['vecorized_graphics']['mean']
        std = self.normalization_parameter['vecorized_graphics']['std']
        return vectorized_graphics*std+mean
            
    def denormalize_sensors(self,sensors):
        mean = self.normalization_parameter['sensors']['mean']
        std = self.normalization_parameter['sensors']['std']

        normalized_sensors = sensors.copy()
        
        normalized_sensors[0] = normalized_sensors[0]*std[0]+mean[0]
        normalized_sensors[2] = normalized_sensors[2]*std[2]+mean[2]
        normalized_sensors[3] = normalized_sensors[3]*std[3]+mean[3]

        return normalized_sensors

    def compile(self):
        if self.model_autoencoder is not None:
            self.model_autoencoder.compile(optimizer=self.keras.optimizers.Adam(lr=self.config['autoencoder']['lr']), loss=self.config['autoencoder']['loss'], metrics=['accuracy'])
        
        if self.model_transfer_function is not None:
            self.model_transfer_function.compile(optimizer=self.keras.optimizers.Adam(lr=self.config['transfer_function']['lr']), loss=self.config['transfer_function']['loss'], metrics=['accuracy'])

        if self.model_q is not None:
            self.model_q.compile(optimizer=self.keras.optimizers.Adam(lr=self.config['q_function']['lr']), loss=self.config['q_function']['loss'], metrics=['accuracy'])
        

    def save(self,model_directory='model',
                autoencoder_model=True,autoencoder_weights=True,
                transfer_function_model=True,transfer_function_weights=True,
                q_model=True,q_weights=True,
                normalization_parameter=True):
        
        #check directory
        if not os.path.exists('{}/'.format(model_directory)):
            os.mkdir('{}/'.format(model_directory))

        #save models
        if autoencoder_model:
            with open('{}/{}'.format(model_directory,self.constant_config['autoencoder']['model']), 'w') as f:
                f.write(self.model_autoencoder.to_json())
            with open('{}/{}'.format(model_directory,self.constant_config['encoder']['model']), 'w') as f:
                f.write(self.model_encoder.to_json())
            with open('{}/{}'.format(model_directory,self.constant_config['decoder']['model']), 'w') as f:
                f.write(self.model_decoder.to_json())
        if autoencoder_weights:
            self.model_autoencoder.save_weights('{}/{}'.format(model_directory,self.constant_config['autoencoder']['weights']))
            self.model_encoder.save_weights('{}/{}'.format(model_directory,self.constant_config['encoder']['weights']))
            self.model_decoder.save_weights('{}/{}'.format(model_directory,self.constant_config['decoder']['weights']))
        
        if transfer_function_model:
            with open('{}/{}'.format(model_directory,self.constant_config['transfer_function']['model']), 'w') as f:
                f.write(self.model_transfer_function.to_json())
        if transfer_function_weights:
            self.model_transfer_function.save_weights('{}/{}'.format(model_directory,self.constant_config['transfer_function']['weights']))
        
        if q_model:
            with open('{}/{}'.format(model_directory,self.constant_config['q_function']['model']), 'w') as f:
                f.write(self.model_q.to_json())
        if q_weights:
            self.model_q.save_weights('{}/{}'.format(model_directory,self.constant_config['q_function']['weights']))
        
        if normalization_parameter:
            with open('{}/{}'.format(model_directory,self.constant_config['normalization_parameters']), 'w') as fp:
                json.dump(self.normalization_parameter, fp)

    def load(self,model_directory='model',
                autoencoder_model=True,autoencoder_weights=True,
                transfer_function_model=True,transfer_function_weights=True,
                q_model=True,q_weights=True,
                normalization_parameter=True):
        
        #check directory
        if not os.path.exists('{}/'.format(model_directory)):
            os.mkdir('{}/'.format(model_directory))

        #load models
        if autoencoder_model:
            with open('{}/{}'.format(model_directory,self.constant_config['autoencoder']['model']), 'r') as f:
                self.model_autoencoder = self.keras.models.model_from_json(f.read())
            with open('{}/{}'.format(model_directory,self.constant_config['encoder']['model']), 'r') as f:
                self.model_encoder = self.keras.models.model_from_json(f.read())
            with open('{}/{}'.format(model_directory,self.constant_config['decoder']['model']), 'r') as f:
                self.model_decoder = self.keras.models.model_from_json(f.read())
        else:
            self.model_autoencoder = None
            self.model_encoder = None
            self.model_decoder = None
        
        if transfer_function_model:
            with open('{}/{}'.format(model_directory,self.constant_config['transfer_function']['model']), 'r') as f:
                self.model_transfer_function = self.keras.models.model_from_json(f.read())
        else:
            self.model_transfer_function = None
        
        if q_model:
            with open('{}/{}'.format(model_directory,self.constant_config['q_function']['model']), 'r') as f:
                self.model_q = self.keras.models.model_from_json(f.read())
        else:
            self.model_q = None
        
        self.compile()

        if autoencoder_weights:
            self.model_autoencoder.load_weights('{}/{}'.format(model_directory,self.constant_config['autoencoder']['weights']))
            self.model_encoder.load_weights('{}/{}'.format(model_directory,self.constant_config['encoder']['weights']))
            self.model_decoder.load_weights('{}/{}'.format(model_directory,self.constant_config['decoder']['weights']))

        if transfer_function_weights:
            self.model_transfer_function.load_weights('{}/{}'.format(model_directory,self.constant_config['transfer_function']['weights']))

        if q_weights:
            self.model_q.load_weights('{}/{}'.format(model_directory,self.constant_config['q_function']['weights']))
        
        if normalization_parameter:
            with open('{}/{}'.format(model_directory,self.constant_config['normalization_parameters']), 'r') as fp:
                self.normalization_parameter = json.load(fp)

